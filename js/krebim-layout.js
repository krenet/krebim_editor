jQuery(window).on("load", function() {
    $('body').attr('data-right-sidebar', 'show');
    $('body').attr('data-sidebar-style') === "mini" ? $(".right-hamburger").addClass('is-active') : $(".right-hamburger").removeClass('is-active')    
});


(function($) {
    $(".right-nav-control").on('click', function() {

        $('#main-wrapper').toggleClass("right-menu-toggle");

        $(".right-hamburger").toggleClass("is-active");

        $('body').attr('data-right-sidebar') === "show" ? $('body').attr('data-right-sidebar', '') : $('body').attr('data-right-sidebar', 'show');
    });

    var e=function(e){
        var t=e.length?e:$(e.target),a=t.data("output");
        window.JSON?a.val(window.JSON.stringify(t.nestable("serialize"))):a.val("JSON browser support required for this demo.")
    };
    $("#modelList").nestable({group:1}).on("change",e),
    $("#deviceList").nestable({group:1}).on("change",e),
    e($("#modelList").data("output",$("#nestable-output"))),
    e($("#deviceList").data("output",$("#nestable2-output"))),
    $("#nestable-menu").on("click",function(e){
        var t=$(e.target).data("action");
        "expand-all"===t&&$(".dd").nestable("expandAll"),
        "collapse-all"===t&&$(".dd").nestable("collapseAll")});
    
    
})(jQuery);
