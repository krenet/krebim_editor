var tileList = new Array();
	
const viewer = new Cesium.Viewer('cesiumContainer', {
    shadows: true
});
viewer.scene.globe.depthTestAgainstTerrain = true;
var tileset = new Cesium.Cesium3DTileset({
    url: './data/tileset.json'
});

viewer.scene.primitives.add(tileset);

var center = Cesium.Cartesian3.fromDegrees(103.74612842336977, 1.3383640950406668, 80.0);

viewer.camera.flyTo({
    destination : center,
    orientation : {
        heading : Cesium.Math.toRadians(180.0),
        pitch : Cesium.Math.toRadians(-25.0),
        roll : 0,
    }
});


tileset.tileLoad.addEventListener((tile)=>{
    for(var inx = 0;inx<tile.content.featuresLength ;++inx){
        tileList.push(tile.content.getFeature(""+inx));
    }
    console.log('tileList : ', tileList);
});

